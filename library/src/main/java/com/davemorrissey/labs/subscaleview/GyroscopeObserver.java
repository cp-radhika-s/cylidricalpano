package com.davemorrissey.labs.subscaleview;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.Matrix;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;

/**
 * Created by gjz on 21/12/2016.
 */

public class GyroscopeObserver implements SensorEventListener {
    private SensorManager mSensorManager;

    // For translate nanosecond to second.
    private static final float NS2S = 1.0f / 1000000000.0f;

    // The time in nanosecond when last sensor event happened.
    private long mLastTimestamp;

    // The radian the device already rotate along y-axis.
    private double mRotateRadianY;

    // The radian the device already rotate along x-axis.
    private double mRotateRadianX;
    // The maximum radian that the device should rotate along x-axis and y-axis to show image's bounds
    // The value must between (0, π/2].
    private double mMaxRotateRadian = Math.PI / 2;

    // The PanoramaImageViews to be notified when the device rotate.
    private SubsamplingScaleImageView mView;
    private WindowManager windowManager;

    public void registerSensor(Context context) {
        windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
//        SensorManager mSensorManager = (SensorManager) context
//                .getSystemService(Context.SENSOR_SERVICE);
//        Sensor sensor = null;// mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
//
//        if (sensor == null) {
//            Log.e("TAg", "TYPE_ROTATION_VECTOR sensor not support!");
//        } else {
//            mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME, null);
//        }

        activeAccelerometerAndMagnetometer(context);

    }

    private void activeAccelerometerAndMagnetometer(Context context) {

        mSensorManager = (SensorManager) context
                .getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (sensor == null) {
            Log.e("TAG", "TYPE_ACCELEROMETER sensor not support!");
            return;
        } else {

            mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME, null);
        }
        Sensor magneticSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        if (magneticSensor == null) {
            Log.e("TAG", "TYPE_MAGNETIC_FIELD sensor not support!");
        } else {
            mSensorManager.registerListener(this, magneticSensor, SensorManager.SENSOR_DELAY_GAME, null);
        }


        mLastTimestamp = 0;
        mRotateRadianY = mRotateRadianX = 0;
    }


    public void unregister() {
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this);
            mSensorManager = null;
        }
    }

    public void addPanoramaImageView(SubsamplingScaleImageView view) {
        mView = view;
    }

    private float[] mSensorialRotationAccelerometerData = new float[3];
    private float[] mSensorialRotationRotationMatrix = new float[16];
    private float[] mSensorialRotationOrientationData = new float[3];
    static final float ALPHA = 0.1f;

    private float[] magSensorVal = new float[3];

    protected float[] lowPass(float[] input, float[] output) {
        if (output == null) return input;
        for (int i = 0; i < input.length; i++) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }

        // Log.e("Motion", Arrays.toString(output));
        return output;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //  Log.e("event", "x: " + event.values[0] + ",Y: " + event.values[1] + ",z: " + event.values[2]);
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                if (mSensorialRotationAccelerometerData != null) {
                    mSensorialRotationAccelerometerData = lowPass(event.values, mSensorialRotationAccelerometerData);
                }
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                if (mSensorialRotationAccelerometerData != null) {
                    magSensorVal = lowPass(event.values, magSensorVal);
                    SensorManager.getRotationMatrix(mSensorialRotationRotationMatrix, null, mSensorialRotationAccelerometerData, magSensorVal);
                    SensorManager.remapCoordinateSystem(mSensorialRotationRotationMatrix, SensorManager.AXIS_Z, SensorManager.AXIS_X, mSensorialRotationRotationMatrix);
                    SensorManager.getOrientation(mSensorialRotationRotationMatrix, mSensorialRotationOrientationData);
                    // Log.e("Sensor", Arrays.toString(mSensorialRotationOrientationData));
                    Matrix.rotateM(mSensorialRotationRotationMatrix, 0, 90.0F, 1.0F, 0.0F, 0.0F);

                    mView.setTranslation(mSensorialRotationOrientationData);

                }
                break;
        }

    }

    private void configureDeviceAngle() {
        // Log.e("sensor", windowManager.getDefaultDisplay().getRotation() + "--");

        switch (windowManager.getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_0: // Portrait
                break;
            case Surface.ROTATION_90: // Landscape
                SensorManager.remapCoordinateSystem(mSensorialRotationRotationMatrix, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, mSensorialRotationRotationMatrix);
                break;
            case Surface.ROTATION_180: // Portrait
                SensorManager.remapCoordinateSystem(mSensorialRotationRotationMatrix, SensorManager.AXIS_MINUS_X, SensorManager.AXIS_MINUS_Y, mSensorialRotationRotationMatrix);
                break;
            case Surface.ROTATION_270: // Landscape
                SensorManager.remapCoordinateSystem(mSensorialRotationRotationMatrix, SensorManager.AXIS_MINUS_Y, SensorManager.AXIS_X, mSensorialRotationRotationMatrix);
                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void setMaxRotateRadian(double maxRotateRadian) {
        if (maxRotateRadian <= 0 || maxRotateRadian > Math.PI / 2) {
            throw new IllegalArgumentException("The maxRotateRadian must be between (0, π/2].");
        }
        this.mMaxRotateRadian = maxRotateRadian;
    }
}
