package com.davemorrissey.labs.subscaleview.test.basicfeatures;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.davemorrissey.labs.subscaleview.GyroscopeObserver;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.davemorrissey.labs.subscaleview.test.R;
import com.davemorrissey.labs.subscaleview.test.R.id;

public class BasicFeaturesActivity extends FragmentActivity {
    GyroscopeObserver gyroscopeObserver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pages_activity);
        SubsamplingScaleImageView view = findViewById(id.imageView);

        gyroscopeObserver = new GyroscopeObserver();
        gyroscopeObserver.addPanoramaImageView(view);


        view.setMinimumScaleType(SubsamplingScaleImageView.SCALE_TYPE_START);
        view.setOrientation(SubsamplingScaleImageView.ORIENTATION_90);
        view.setImage(ImageSource.asset("img.jpg"));

    }


    @Override
    protected void onResume() {
        super.onResume();
        gyroscopeObserver.registerSensor(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        gyroscopeObserver.unregister();
    }
}
